import com.sun.applet2.preloader.event.DownloadEvent;
import com.sun.deploy.uitoolkit.impl.awt.ui.DownloadWindow;

import javax.jnlp.DownloadService;
import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Demo {
    public static void main(String[] args) throws IOException {
        String Url;


        BufferedReader intFile = new BufferedReader(new FileReader("intFile.txt"));

        BufferedWriter outFile = new BufferedWriter(new FileWriter("outFile.txt"));
        while ((Url = intFile.readLine()) != null) {
            URL url = new URL(Url);

            String result;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                result = bufferedReader.lines().collect(Collectors.joining("\n"));
            }
            Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
            Matcher matcher = email_pattern.matcher(result);
            int i = 0;
                while (matcher.find() && i < 2 ) {
                    outFile.write(matcher.group() + "\r\n");
                    i++;
                }
        }

        intFile.close();
        outFile.close();

        BufferedReader intFiles = new BufferedReader(new FileReader("outFile.txt"));
        String music;
        int count = 0;
        while ((music = intFiles.readLine()) != null){
            downloadUsingNIO(music, "C:\\Users\\roman\\Documents\\kvalik\\music"+ String.valueOf(count) +".mp3");
            count++;
        }
        intFiles.close();

    }
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}

